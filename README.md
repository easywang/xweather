XWeather  
基于Yii的天气预报程序  

分成两部分：  
1.后台的抓取程序  
2.API  

这是一个完整的应用程序，拿来即可使用。  

可以参见我的教程  
我的独立博客：http://www.xiongchuan.org/blog/archives/807  
OSC的博客：  http://my.oschina.net/hierick/blog/136749  

联系我:  
博客：http://www.xiongchuan.org  
微博：http://weibo.com/hierick  
豆瓣：http://www.douban.com/people/hierick/  

捐赠我：https://me.alipay.com/xiongchuan
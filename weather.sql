/*
SQLyog Trial v10.5 
MySQL - 5.5.28-log : Database - x_weather
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`x_weather` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `x_weather`;

/*Table structure for table `w_china` */

CREATE TABLE `w_china` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `name_type` set('province','city','station') NOT NULL,
  `weather_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '中国天气网城市id',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2953 DEFAULT CHARSET=utf8;

/*Table structure for table `w_weather` */

CREATE TABLE `w_weather` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `weather_id` int(10) unsigned NOT NULL,
  `weather` text NOT NULL,
  `update_time` int(10) unsigned NOT NULL COMMENT '更新时间',
  `is_new` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `weather_id` (`weather_id`)
) ENGINE=MyISAM AUTO_INCREMENT=620957 DEFAULT CHARSET=utf8;

<?php

class GetChinaCommand extends CConsoleCommand{
        
    /**
     * 获取中国省级信息
     */
    public function getChinaData(){
        $url = "http://www.weather.com.cn/data/city3jdata/china.html";
        echo "china url:$url\n";
        $result = Yii::app()->curl->run($url);
        $data = json_decode($result,true);
        $provinces = array();
        foreach($data as $id=>&$name){
            $provinces[] = array(
                ':name' => $name,
                ':name_type' => 'province',
                ':weather_id' => $id,
            );
        }
        return $provinces;
    }
    
    /**
     * 获取指定省的城市
     */
    public function getCityData($weather_id){
        $url = "http://www.weather.com.cn/data/city3jdata/provshi/{$weather_id}.html";
        echo "city url:$url\n";
        $result = Yii::app()->curl->run($url);
        $data = json_decode($result,true);
        $citys = array();
        foreach($data as $id=>&$name){
            if(strlen($id)==2){
                $wid = $weather_id.$id;
            }else{
                $wid = $id;
            }
            $citys[] = array(
                ':name' => $name,
                ':name_type' => 'city',
                ':weather_id' => $wid,
            );
        }
        return $citys;
    }
    
    /**
     * 获取指定城市的区
     */
    public function getStationData($weather_id){
        $url = "http://www.weather.com.cn/data/city3jdata/station/{$weather_id}.html";
        echo "station url:$url\n";
        $result = Yii::app()->curl->run($url);
        $data = json_decode($result,true);
        $stations = array();
        foreach($data as $id=>&$name){
            if(strlen($id)==2){
                if(in_array($weather_id, array(1010100,1010200,1010300,1010400))){
                    $wid = substr($weather_id,0, -2).$id.'00';
                }else{
                    $wid = $weather_id.$id;
                }
            }else{
                $wid = $id;
            }
            $stations[] = array(
                ':name' => $name,
                ':name_type' => 'station',
                ':weather_id' => $wid,
            );
        }
        return $stations;
    }
    
    public function run(){
        $china = $this->getChinaData();
        $sqls = array();
        $sql = "INSERT INTO {{china}} SET name=:name,name_type=:name_type,weather_id=:weather_id";
        $command = Yii::app()->db->createCommand($sql);
        foreach ($china as &$province) {
            $command->bindValues($province)->execute();//插入省
            $citys = $this->getCityData($province[':weather_id']);
            foreach ($citys as &$city) {
                $command->bindValues($city)->execute();//插入市
                $stations = $this->getStationData($city[':weather_id']);
                foreach ($stations as &$station) {
                    $command->bindValues($station)->execute();//插入地区
                }
                //$city['stations'] = $stations;
            }
            //$province['citys'] = $citys;
        }
        echo "end\n";
    }
 
}
    